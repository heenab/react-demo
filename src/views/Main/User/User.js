import React, { PropTypes as T } from 'react'
import {Panel, Col, Button} from 'react-bootstrap'
import styles from './styles.module.css'
import { Link } from 'react-router'
import jwtDecode from 'jwt-decode'
import md5 from 'md5'
import { API_URL } from './../../../utils/constants'

function getUserListItem (user) {
  const avatarUrl = `https://www.gravatar.com/avatar/${md5(user.email).toLowerCase().trim()}s=200`
  return (
    <Panel key={user._id}>
      <Col sm={1}>
        <img src={avatarUrl} />
      </Col>
      <Col sm={11}>
        <h3>{ user.first_name + ' ' + user.last_name }</h3>
        <p><i className="glyphicon glyphicon-envelope"></i> { user.email }</p>
        <p><i className="glyphicon glyphicon-briefcase"></i> { user.company }</p>
      </Col>
    </Panel>
  )
}

export class User extends React.Component {
  static contextTypes = {
    router: T.object
  }

  constructor(props, context) {
    super(props, context)
    this.state = {
      users: []
    }
    this.props.auth.fetch(`${API_URL}/user`).then(data => this.setState({ users: data }))
  }

  onAddUserClick() {
    this.context.router.push('/user/new')
  }

  render() {
    let userList;
    if (this.state.users) {
      userList = this.state.users.map(user => getUserListItem(user))
    }

    const { auth } = this.props;

    return (
      <div>
        <h2>Front End Masters Users</h2>
        { auth.isAuthenticated() && auth.isAdmin() && 
          <Button bsStyle="primary" onClick={this.onAddUserClick.bind(this)}>
            <i className="glyphicon glyphicon-plus"></i> Add User
          </Button>
        }
        <ul>
          {userList}
        </ul>
      </div>
    )
  }
}

export default User
